# Zeus-ics

Micro-service to get your ics from zeus with your credentials.

**For educational purposes only ofc ;)**

## How to use with docker

**Mandatory environment variable**:

- `ZEUS_USERNAME`: Your bocal email address
- `ZEUS_PASSWORD`: Your bocal password :)
- `ZEUS_GID`: Which group will be exposed as ics

This will start a gunicorn service exposing internally port 8000

Command example:

```
docker run -d --name zeus-ics -p 127.0.0.1:8000:8000 \
    -e ZEUS_USERNAME=login_x \
    -e ZEUS_PASSWORD=mylovelybocalpassword \
    -e ZEUS_GID=23 \
    registry.gitlab.com/synapze/zeus-ics:<sha-1>
```
