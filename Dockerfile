FROM python:3.8.10-slim

RUN apt update && apt install -y --no-install-recommends \
    firefox-esr \
    curl \
    && rm -rf /var/lib/apt/lists/*

RUN curl -L https://github.com/mozilla/geckodriver/releases/download/v0.30.0/geckodriver-v0.30.0-linux64.tar.gz | tar zx && \
    mv geckodriver /usr/bin

COPY . /app

WORKDIR /app

RUN pip install -r ./requirements.txt

RUN pip install gunicorn==20.1.0

CMD ["gunicorn", "--access-logfile", "-", "--timeout", "300", "-b", "0.0.0.0:8000", "zeus_ics:wsgi"]
