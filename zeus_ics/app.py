from os import environ
from datetime import datetime
from typing import Optional

from .zeus import Zeus
from .zeus.zeus import ZeusException
from ics import Calendar

import json
from dateutil.relativedelta import relativedelta

def get_calendar(zeus: Zeus, group: int) -> Optional[Calendar]:
    now = datetime.now()
    monthdelta = 4
    cal = None

    for i in range(-monthdelta, monthdelta):
        rdate = now+relativedelta(months=i)
        reservation = zeus.get_reservation(group, rdate.year, rdate.month)
        cal = zeus.from_reservation_to_ics(reservation, cal)
    return cal

def create_app(auth_at_start: bool = False):
    login = environ['ZEUS_USERNAME']
    password = environ['ZEUS_PASSWORD']
    group = int(environ['ZEUS_GID'])

    zeus = Zeus(login, password)

    # Force to be authenticated at start, avoid long request
    if auth_at_start:
        zeus.authenticate()

    def stats(start_response):
        headers = [('Content-type', 'application/json; charset=utf-8')]
        status = '200 OK'
        start_response(status, headers)
        statistics = {
                'request_counter': zeus.req_counter,
                'request_ics_counter': zeus.req_ics_counter,
            }
        return json.dumps(statistics).encode('utf-8')

    def wsgi(env, start_response):
        if env["REQUEST_METHOD"] not in ['GET']:
            start_response('404 Not Found', [])
            return None

        if env["PATH_INFO"] == "/stats":
            yield stats(start_response)
        elif env["PATH_INFO"] in ["", "/"]:
            zeus.req_ics_counter += 1
            headers = [('Content-type', 'text/calendar; charset=utf-8')]
            response = ""

            status = '200 OK'
            try:
                cal = get_calendar(zeus, group)
                response = str(cal)
            except ZeusException:
                status = '500 Internal Server Error'

            start_response(status, headers)
            yield str(response).encode('utf-8')
    return wsgi
