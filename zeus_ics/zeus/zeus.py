from typing import Optional
import requests
import json
import urllib.parse
from datetime import datetime
from functools import wraps
import logging

from time import sleep
from os.path import devnull
from requests.models import Response

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException

from ics import Calendar, Event, Attendee

from memoization import cached

# Exception

class ZeusException(Exception):
    """base exception for Zeus"""

class ZeusAuthenticateFailure(ZeusException):
    """raised when could't get token"""

class ZeusRequestException(ZeusException):
    def __init__(self, response: Response, *args, **kwargs):
        self.response = response
        super().__init__(*args, **kwargs)

    def __str__(self) -> str:
        sc = self.response.status_code
        body = self.response.text
        return super().__repr__() + f" status_code: {sc} body: {body}"

# Class

class Zeus:
    API_ENDPOINT: str = "https://zeus.ionis-it.com"

    def __init__(self, login: str, password: str):
        self.login = login
        self.password = password
        self.token = None

        # Dictionary to store memoization request
        self.memoization = dict()
        self.teachers_by_courseName = dict()

        self.authenticated = False

        # Stats
        self.req_counter = 0
        self.req_ics_counter = 0

    def authenticate(self, headless = True):
        opts = webdriver.FirefoxOptions()
        opts.headless = headless
        driver = webdriver.Firefox(options=opts, service_log_path=devnull)
        try:
            login_url = urllib.parse.urljoin(self.API_ENDPOINT, "/login")
            driver.get(login_url)
            sleep(5)
            btn = driver.find_element_by_class_name("btn-office")
            btn.click()

            mail = WebDriverWait(driver, 10).until(
                    EC.presence_of_element_located((By.NAME, "loginfmt"))
                )
            mail.send_keys(self.login)
            next = driver.find_element_by_id('idSIButton9')
            next.click()

            passwd = WebDriverWait(driver, 10).until(
                    EC.presence_of_element_located((By.NAME, "Password"))
                )

            passwd.send_keys(self.password)
            submit = driver.find_element_by_id('submitButton')

            submit.click()

            dontshowthisno = WebDriverWait(driver, 10).until(
                    EC.presence_of_element_located((By.ID, "idBtn_Back"))
                )
            dontshowthisno.click()

            WebDriverWait(driver, 10).until(EC.title_contains("Zeus"))

            sleep(5)

            auth = driver.execute_script("return window.localStorage.getItem('ZEUS-AUTH');")
            self.token = json.loads(auth)['token']
        except TimeoutException:
            now = datetime.now()
            driver.save_screenshot(f"/tmp/selenium-timeout-{str(now)}.png")
            logging.error("authenticate failure: timeout")
        finally:
            driver.close()

    def _prepare_headers(self, accept: str = 'text/plain') -> dict:
        """return headers for requests"""
        return {
                'Authorization': 'Bearer ' + str(self.token),
                'Content-Type': 'application/json',
                'accept': accept,
            }

    def _perform_request(self, method:str, url:str, payload=None, **kwargs):
        headers = kwargs.get('headers')
        if not headers:
            headers = self._prepare_headers(**kwargs)
        if not hasattr(requests, method) and callable(getattr(requests, method)):
            msg = f"`{method}` doesn't exists in requests or this not callable"
            raise Exception(msg)
        req_fn = getattr(requests, method)
        endpoint = urllib.parse.urljoin(self.API_ENDPOINT, url)

        self.req_counter += 1

        logging.info(method.upper(), url)

        return  req_fn(endpoint, headers=headers, data=payload)

    @cached(ttl=60 * 30) # 30 minutes caching
    def _requests(self, method:str, url:str, payload=None,
            raise_for_status: bool = True, **kwargs) -> Response:

        response: Response = self._perform_request(method, url, payload,
                **kwargs)

        # Verify if request didn't return Unauthorized
        if response.status_code == 401:
            # Mark as not authenticated
            self.authenticated = False
            self.authenticate()
            # Retry request
            response = self._perform_request(method, url, payload, **kwargs)

        if raise_for_status and response.status_code >= 400:
            raise ZeusRequestException(response)

        return response

    def test_auth(self) -> bool:
        """use zeus endpoint to test if client is authenticated and token is
        valid"""
        if self.authenticated:
            return True

        req = self._requests('get', "api/User/testAuth", raise_for_status=False)
        self.authenticated = req.status_code == 200
        return self.authenticated

    # def get_ics_groups(self, group: int = GISTRE_GID):
    #     hed = { 'accept': 'text/calendar' }
    #     req = self._requests('get', f"api/group/{group}/ics", headers=hed)
    #     print(req.text)

    def get_group(self) -> dict:
        req = self._requests('get', "group")
        return req.json()

    def get_reservation(self, group: int, year: int, month: int) -> list:
        """get all the reservation for the `month`"""
        startDate = datetime(year, month, 1)
        if month + 1 == 13:
            endDate = datetime(year + 1, 1, 1)
        else:
            endDate = datetime(year, month + 1, 1)
        payload = {
                'groups': [
                    group,
                    ],
                'startDate': startDate.isoformat() + 'Z',
                'endDate': endDate.isoformat() + 'Z',
            }
        req = self._requests('post', 'api/reservation/filter/displayable',
                payload=json.dumps(payload))
        return req.json()

    def get_referent_teacher(self) -> dict:
        """get all ReferentTeacher in dictionary by idCourse as key"""
        req = self._requests('get', 'api/referentteacher')
        # Reformat to be a dictionary indexed by courseId
        data = dict()

        for rt in req.json():
            if not data.get(rt['idCourse']):
                data[rt['idCourse']] = [ rt['idTeacher'] ]
            else:
                data[rt['idCourse']].append(rt['idTeacher'])

        return data

    def get_teachers(self) -> dict:
        """get all Teacher"""
        req = self._requests('get', 'api/teacher/public')
        # Reformat to be a dictionary indexed by id
        data = {
                t['id']: {
                    'name': t['name'],
                    'firstname': t['firstname'],
                    'isInternal': t['isInternal'],
                } for t in req.json()
            }
        return data

    @cached(ttl=90 * 30) # 90 minutes caching
    def get_reservation_details(self, reservation_id: int) -> dict:
        """get reservation details"""
        req = self._requests('get', f"api/reservation/{reservation_id}/details")
        data = req.json()
        name = data['name']
        teachers = [
                f"{t['firstname']} {t['name']}"
                for t in data['teachers']
            ]
        self.teachers_by_courseName[name] = teachers
        return data

    def get_teachers_for_course(self, course_id: int) -> list:
        """get teachers for course_id"""
        referentteacher = self.get_referent_teacher()
        teachers = self.get_teachers()

        # TODO: Since i'm not handling error case i want it to crash
        teacherIds = referentteacher[course_id]
        teacher_names = list()
        for t_id in teacherIds:
            firstname = teachers[t_id]['firstname']
            name = teachers[t_id]['name']
            teacher_names.append(f"{firstname} {name}")
        return teacher_names

    def from_reservation_to_ics(self, data: list, calendar: Calendar = None) -> Calendar:
        if not calendar:
            calendar = Calendar()

        for reservation in data:
            rooms = ' '.join([ room['name'] for room in reservation['rooms']])
            attendees = [group['name'] for group in reservation['groups']]
            name = reservation['name']

            teachers: list = []
            link: Optional[str] = None

            isOnline = reservation['isOnline']

            # Lookup for url
            if isOnline:
                details = self.get_reservation_details(reservation['idReservation'])
                link = details['url']

            if name in self.teachers_by_courseName:
                teachers = self.teachers_by_courseName.get(name, [])
            else:
                if reservation['idCourse']:
                    teachers = self.get_teachers_for_course(reservation['idCourse'])
                    self.teachers_by_courseName[name] = teachers
                else:
                    details = self.get_reservation_details(reservation['idReservation'])

            event = Event()

            event.name = name
            event.begin = reservation['startDate']
            event.end = reservation['endDate']
            event.location = rooms
            event.attendees = { Attendee(t) for t in teachers }
            event.url = link

            # Description
            desc = 'Groups: ' + ' '.join(attendees)

            event.description = desc

            calendar.events.add(event)
        return calendar
